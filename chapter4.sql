/* #1 */
select
  name
from
  world
where 
  population > (
    select
      population
    from
      world
    where
      name = 'Russia'
  );


/* #2 */
select 
  name, continent
from 
  world
where 
  continent in (
    select 
      continent
    from
      world
    where 
      name in ('Belize', 'Belgium')
  );

/* #3 */
select 
  name
from
  world
where 
  continent = 'Europe' and gdp/population > (
    select
      gdp/population
    from
      world
    where
      name = 'United Kingdom'
  );

/* #4 */
select
  name, population
from
  world
where
  population > (
    select 
      population
    from
      world
    where
      name = 'Canada'
  ) and population < (
    select
      population
    from
      world
    where
      name = 'Poland'
  );

/* #5 */
select
  name
from
  world
where
  gdp > ALL (
    select
      gdp
    from 
      world
    where
      continent = 'Europe'
  );

/* #6 */
select
  continent, name, area
from
  world x
where
  area >= ALL (
    select
      area
    from
      world y
    where 
      x.continent = y.continent
  );

/* #7 */
select 
  name, continent, population
from
  world x
where
  x.continent not in (
    select
      y.continent
    from
      world y
    where
      y.population >= 25000000
  );

/* #8 */
select
  name, continent
from
  world x
where
  x.population > ALL (
    select 
      3 * y.population
    from
      world y
    where
      y.continent = x.continent and
      y.name <> x.name
  );
